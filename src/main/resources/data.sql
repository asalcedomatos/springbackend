-- INSER PERSONAS
INSERT INTO persona (id, apellido,apellido2,direccion,documento,email,nombre,nombre2,ocupacion,telefono,tipo,tipopersona) VALUES
(1,'tordecilla','paez','pozon','1235041932','eduarotewar@gmail.com','eduardo','','developer','30039882392','jefe','natural');

INSERT INTO persona (id,apellido,apellido2,direccion,documento,email,nombre,nombre2,ocupacion,telefono,tipo,tipopersona) VALUES
(2,'Gonzalez','Atencio','el centro','123456789','eduardotewar@gmail.com','jorge','de jesus','policia','23456789','Cliente','natural');

INSERT INTO persona (id,apellido,apellido2,direccion,documento,email,nombre,nombre2,ocupacion,telefono,tipo,tipopersona) VALUES
(3,'Salcedo','Matos','camino e medio','123456789','asalcedos@gmail.com','Arnold','nodita','abogado','123456790','Abogado',NULL);

-- INSERT USUARIOS

INSERT INTO usuarios (id,contrasena,persona,usuario) VALUES
(1,'123456789',1,'test_jefe')
,(2,'123456789',2,'test_client')
,(3,'123456789',3,'test_abogado');