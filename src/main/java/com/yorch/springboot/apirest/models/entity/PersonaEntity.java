package com.yorch.springboot.apirest.models.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
/*
import java.util.HashSet;
import java.util.Set;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import lombok.Data;
import lombok.EqualsAndHashCode;*/
/*
@Data
@EqualsAndHashCode(exclude="procesos")*/

@Entity
@Table(name="persona")
public class PersonaEntity implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private String tipo;
	private String nombre;
	private String nombre2;
	private String apellido;
	private String apellido2;
	private String documento;
	private String email;
	private String direccion;
	private String telefono;
	private String ocupacion;
	private String tipopersona;
	
	/*
	@ManyToOne
	@JoinTable(name="persona_has_proceso",
	joinColumns = @JoinColumn(name="abogado",referencedColumnName="id"),
	inverseJoinColumns=@JoinColumn(name="proceso",referencedColumnName="id"))
	private Set<Proceso> procesos;*/
		
	public PersonaEntity() {
		
	}


	public PersonaEntity(Long id, String tipo, String nombre, String nombre2, String apellido, String apellido2,
			String documento, String email, String direccion, String telefono, String ocupacion, String tipopersona) {
		this.id = id;
		this.tipo = tipo;
		this.nombre = nombre;
		this.nombre2 = nombre2;
		this.apellido = apellido;
		this.apellido2 = apellido2;
		this.documento = documento;
		this.email = email;
		this.direccion = direccion;
		this.telefono = telefono;
		this.ocupacion = ocupacion;
		this.tipopersona = tipopersona;
	}


	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}



	public String getTipo() {
		return tipo;
	}



	public void setTipo(String tipo) {
		this.tipo = tipo;
	}



	public String getNombre() {
		return nombre;
	}



	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



	public String getNombre2() {
		return nombre2;
	}



	public void setNombre2(String nombre2) {
		this.nombre2 = nombre2;
	}



	public String getApellido() {
		return apellido;
	}



	public void setApellido(String apellido) {
		this.apellido = apellido;
	}



	public String getApellido2() {
		return apellido2;
	}



	public void setApellido2(String apellido2) {
		this.apellido2 = apellido2;
	}



	public String getDocumento() {
		return documento;
	}



	public void setDocumento(String documento) {
		this.documento = documento;
	}



	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}



	public String getDireccion() {
		return direccion;
	}



	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}



	public String getTelefono() {
		return telefono;
	}



	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}



	public String getOcupacion() {
		return ocupacion;
	}



	public void setOcupacion(String ocupacion) {
		this.ocupacion = ocupacion;
	}



	public String getTipopersona() {
		return tipopersona;
	}



	public void setTipopersona(String tipopersona) {
		this.tipopersona = tipopersona;
	}


/*
	public Set<Proceso> getProcesos() {
		return procesos;
	}



	public void setProcesos(Set<Proceso> procesos) {
		this.procesos = procesos;
	}*/
	
}
