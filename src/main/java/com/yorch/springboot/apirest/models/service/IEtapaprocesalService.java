package com.yorch.springboot.apirest.models.service;

import java.util.List;

import com.yorch.springboot.apirest.models.entity.EtapasProcesales;

public interface IEtapaprocesalService {
	
	public List<EtapasProcesales> findAll();
	
	public EtapasProcesales findById(long id);
	
	public void delete (long id);
	
	public EtapasProcesales save(EtapasProcesales etapa);
	
	public List<EtapasProcesales> porProceso(int id);
}