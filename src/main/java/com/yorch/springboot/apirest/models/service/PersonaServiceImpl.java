package com.yorch.springboot.apirest.models.service;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.yorch.springboot.apirest.models.dao.IPersonaDao;
import com.yorch.springboot.apirest.models.entity.Persona;
//import com.yorch.springboot.apirest.models.dao.IProcesoDao;
//import com.yorch.springboot.apirest.models.entity.Proceso;
//import com.yorch.springboot.apirest.models.entity.Respuesta;

@Service
public class PersonaServiceImpl implements IPersonaService {
	
	private IPersonaDao personaDao;
	
	
	
	@Autowired
	public PersonaServiceImpl(IPersonaDao personaDao) {
		this.personaDao = personaDao;
	}

	//private IProcesoDao procesoDao;
 
	@Override
	@Transactional(readOnly=true)
	public List<Persona> findAll() {
		
		//List<PersonaEntity> person = (List<PersonaEntity>) personaDao.findAll();
		//return new Respuesta(200, "Listado Cargado", person);
		return (List<Persona>) personaDao.findAll();
	}
	
	@Override
	@Transactional
	public void delete(Long id) {
		if(personaDao.findById(id)!=null) {
			personaDao.deleteById(id);
		}else {
			throw new RuntimeException("No se pudo eliminar la persona");
		}
		
	}
	
	@Override
	@Transactional(readOnly=true)
	public Persona findById(long id) {
		return personaDao.findById(id).orElse(null);
		
		/*PersonaEntity persona;
		Respuesta response = new Respuesta();
		try {
			persona = personaDao.findById(id).orElse(null);
			response.setMensaje(persona!=null?"PersonaEntity Cargada":"persona no encontrada");
			response.setCodigo(persona!=null?200:202);
			response.setData(persona);
		} catch (Exception e) {
			response.setCodigo(500);
			response.setMensaje(e.getMessage());
			response.setData(null);
		}
		return response;*/
	}
	
	@Override
	@Transactional
	public Persona save(Persona persona) {
		// TODO Auto-generated method stub
		/*Respuesta response = new Respuesta();
		try {
			response.setCodigo(200);
			response.setMensaje("PersonaEntity Guardada Correctamente");
			response.setData(personaDao.save(persona));
		} catch (Exception e) {
			response.setCodigo(500);
			response.setMensaje(e.getMessage());
			response.setData(null);
		}
		return response;*/
		return personaDao.save(persona);
	}
/*
	@Override
	@Transactional
	public List<Proceso> findLawer(int id) {
		List<Proceso> p = procesoDao.findLawer(id);
		return p!=null?p:null;
		return null;
	}*/

	@Override
	public List<Persona> abogado() {
		// TODO Auto-generated method stub
		return personaDao.abogado();
	}

}

//return (List<PersonaEntity>) personaDao.findAll();