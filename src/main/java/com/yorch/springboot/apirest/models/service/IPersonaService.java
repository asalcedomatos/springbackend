package com.yorch.springboot.apirest.models.service;

import java.util.List;

import com.yorch.springboot.apirest.models.entity.Persona;
//import com.yorch.springboot.apirest.models.entity.Proceso;
//import com.yorch.springboot.apirest.models.entity.Respuesta;

public interface IPersonaService {
	
	public List<Persona> findAll();
	
	public List<Persona>abogado();
	
	public Persona findById(long id);
	
	public void delete(Long id);
	
	public Persona save (Persona persona);
	
	//public List<Proceso> findLawer(int id);

}
