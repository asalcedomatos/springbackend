package com.yorch.springboot.apirest.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.yorch.springboot.apirest.models.dao.IEtapaProcesalDao;
import com.yorch.springboot.apirest.models.entity.EtapasProcesales;

@Service
public class IEtapaprocesalServiceImpl implements IEtapaprocesalService {
	@Autowired
	private IEtapaProcesalDao etapaProcesalDao;
	
	@Override
	@Transactional(readOnly=true)
	public List<EtapasProcesales> findAll() {
		// TODO Auto-generated method stub
		return (List<EtapasProcesales>) etapaProcesalDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public EtapasProcesales findById(long id) {
		// TODO Auto-generated method stub
		return etapaProcesalDao.findById(id).orElse(null);
	}

	@Override
	public void delete(long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public EtapasProcesales save(EtapasProcesales etapa) {
		// TODO Auto-generated method stub
		return etapaProcesalDao.save(etapa);
	}

	@Override
	public List<EtapasProcesales> porProceso(int id) {
		// TODO Auto-generated method stub
		return etapaProcesalDao.porProceso(id);
	}

}
