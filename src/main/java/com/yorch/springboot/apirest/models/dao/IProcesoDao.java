package com.yorch.springboot.apirest.models.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.yorch.springboot.apirest.models.entity.Proceso;

public interface IProcesoDao extends CrudRepository<Proceso, Integer>{
	
	@Query(value="select dd.persona,p.* from proceso  p inner join persona_has_proceso ph on p.id = ph.proceso inner join consultoria_juridica.dte_ddo  dd on ph.id = dd.relacion_proceso where dd.persona =?1", nativeQuery=true)
	List<Proceso> findProcessById(@Param("persona")long id);
	
	@Query(value="select dd.persona,p.* from proceso  p inner join persona_has_proceso ph on p.id = ph.proceso inner join dte_ddo dd on dd.relacion_proceso = ph.id where dd.persona =?1",nativeQuery=true)
	List<Proceso> findLawer(@Param("id") long id);

}
