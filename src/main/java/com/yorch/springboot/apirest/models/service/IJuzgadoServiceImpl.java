package com.yorch.springboot.apirest.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.yorch.springboot.apirest.models.dao.IJuzgadoDao;
import com.yorch.springboot.apirest.models.entity.Juzgado;
@Service
public class IJuzgadoServiceImpl implements IJuzgadoService {
	@Autowired
	private IJuzgadoDao juzgadoDao;

	@Override
	@Transactional(readOnly=true)
	public List<Juzgado> findAll() {
		// TODO Auto-generated method stub
		return (List<Juzgado>) juzgadoDao.findAll();
	}

	@Override
	@Transactional(readOnly=true)
	public Juzgado findById(int id) {
		// TODO Auto-generated method stub
		return juzgadoDao.findById(id).orElse(null);
	}

	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	@Transactional
	public Juzgado save(Juzgado persona) {
		// TODO Auto-generated method stub
		return juzgadoDao.save(persona);
	}

}
