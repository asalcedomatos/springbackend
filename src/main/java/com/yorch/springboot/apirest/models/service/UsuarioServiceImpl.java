package com.yorch.springboot.apirest.models.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.yorch.springboot.apirest.models.dao.*;

import com.yorch.springboot.apirest.models.entity.*;

@Service
public class UsuarioServiceImpl implements IUsuarioService{
	@Autowired
	private IUsuarioDao usuarioDao;

	@Override
	@Transactional(readOnly=true)
	public Respuesta findAll() {
		// TODO Auto-generated method stub
		return new Respuesta(200, "Usuarios todos", usuarioDao.findAll());
	}

	@Override
	@Transactional(readOnly=true)
	public Usuario findById(int id) {
		// TODO Auto-generated method stub
		return usuarioDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public void delete(int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	@Transactional
	public Usuario save(Usuario usuario) {
		// TODO Auto-generated method stub
		return usuarioDao.save(usuario);
	}

	@Override
	public Usuario findbyUP(String usuario, String contrasena) {
		// TODO Auto-generated method stub
		return usuarioDao.findByUP(usuario, contrasena);
	}

}
