package com.yorch.springboot.apirest.models.service;

import java.util.List;

import com.yorch.springboot.apirest.models.entity.Solicitudes;

public interface ISolicitudesService {

	public List<Solicitudes> findAll();
	
	public Solicitudes findById(int id);
	
	public void delete(int id);
	
	public Solicitudes save (Solicitudes solicitudes);

}
