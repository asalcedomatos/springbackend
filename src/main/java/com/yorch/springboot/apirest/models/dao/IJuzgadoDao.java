package com.yorch.springboot.apirest.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.yorch.springboot.apirest.models.entity.Juzgado;

public interface IJuzgadoDao extends CrudRepository<Juzgado, Integer>{

}
