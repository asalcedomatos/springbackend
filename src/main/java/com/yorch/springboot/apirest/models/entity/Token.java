package com.yorch.springboot.apirest.models.entity;

public class Token {
	private Persona persona;
	private Usuario usuario;
	
	public Token() {
		super();
	}
	public Token(Persona persona, Usuario usuario) {
		super();
		this.persona = persona;
		this.usuario = usuario;
	}
	public Persona getPersona() {
		return persona;
	}
	public void setPersona(Persona persona) {
		this.persona = persona;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}
