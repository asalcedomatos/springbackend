package com.yorch.springboot.apirest.models.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name="solicitudes")
public class Solicitudes {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idsolicitudes;
	private String documento;
	private String nombre;
	private String apellido;
	private String telefono;
	private String ciudad;
	private String departamento;
	private String email;
	private String nombre2;
	private String descripcion;
	private String apellido2;
	private String ocupacion;
	private String pais;
	private String estado;
	private int persona;
	
	@Column(insertable = false)
	private Integer abogado;
	public Solicitudes(int idsolicitudes, String documento, String nombre, String apellido, String telefono,
			String ciudad, String departamento, String email, String nombre2, String descripcion,
			String apellido2, String ocupacion, String pais, int persona, Integer abogado,String estado) {
		super();
		this.idsolicitudes = idsolicitudes;
		this.documento = documento;
		this.nombre = nombre;
		this.apellido = apellido;
		this.telefono = telefono;
		this.ciudad = ciudad;
		this.departamento = departamento;
		this.email = email;
		this.nombre2 = nombre2;
		this.descripcion = descripcion;
		this.apellido2 = apellido2;
		this.ocupacion = ocupacion;
		this.pais = pais;
		this.persona = persona;
		this.abogado = abogado;
		this.estado = estado;
	}
	public Solicitudes() {
		super();
	}
	public int getIdsolicitudes() {
		return idsolicitudes;
	}
	public void setIdsolicitudes(int idsolicitudes) {
		this.idsolicitudes = idsolicitudes;
	}
	public String getdocumento() {
		return documento;
	}
	public void setdocumento(String documento) {
		this.documento = documento;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getapellido() {
		return apellido;
	}
	public void setapellido(String apellido) {
		this.apellido = apellido;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getCiudad() {
		return ciudad;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	public String getDepartamento() {
		return departamento;
	}
	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getnombre2() {
		return nombre2;
	}
	public void setnombre2(String nombre2) {
		this.nombre2 = nombre2;
	}
	public String getdescripcion() {
		return descripcion;
	}
	public void setdescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getapellido2() {
		return apellido2;
	}
	public void setapellido2(String apellido2) {
		this.apellido2 = apellido2;
	}
	public String getocupacion() {
		return ocupacion;
	}
	public void setocupacion(String ocupacion) {
		this.ocupacion = ocupacion;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public int getPersona() {
		return persona;
	}
	public void setPersona(int persona) {
		this.persona = persona;
	}
	public Integer getAbogado() {
		return abogado;
	}
	public void setAbogado(Integer abogado) {
		this.abogado = abogado;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
}
