package com.yorch.springboot.apirest.models.dao;


import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.yorch.springboot.apirest.models.entity.Persona;

public interface IPersonaDao extends CrudRepository<Persona, Long>{
	
	@Query(value="select * from persona where tipo = 'Abogado'", nativeQuery=true)
	List<Persona> abogado();
	
	

}
