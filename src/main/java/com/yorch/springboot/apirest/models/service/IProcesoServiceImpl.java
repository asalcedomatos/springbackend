package com.yorch.springboot.apirest.models.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yorch.springboot.apirest.models.dao.IProcesoDao;
import com.yorch.springboot.apirest.models.entity.Proceso;

@Service
public class IProcesoServiceImpl implements IProcesoService{
	@Autowired
	private IProcesoDao procesoDao;

	@Override
	public List<Proceso> findAll() {
		// TODO Auto-generated method stub
		return (List<Proceso>)procesoDao.findAll();
	}

	@Override
	public Proceso findById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Proceso save(Proceso proceso) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Proceso> findProcessById(long id) {
		// TODO Auto-generated method stub
		return procesoDao.findProcessById(id);
	}

	@Override
	public List<Proceso> findLawer(long id) {
		// TODO Auto-generated method stub
		return procesoDao.findLawer(id);
	}

}
