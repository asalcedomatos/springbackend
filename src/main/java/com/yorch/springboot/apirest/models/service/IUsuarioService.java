package com.yorch.springboot.apirest.models.service;

import com.yorch.springboot.apirest.models.entity.Respuesta;
import com.yorch.springboot.apirest.models.entity.Usuario;

public interface IUsuarioService {
public Respuesta findAll();
	
	public Usuario findById(int id);
	
	public void delete(int id);
	
	public Usuario save (Usuario usuario);
	
	public Usuario findbyUP(String usuario,String contrasena);
}
