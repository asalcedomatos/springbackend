package com.yorch.springboot.apirest.models.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.yorch.springboot.apirest.models.entity.Usuario;

public interface IUsuarioDao extends CrudRepository<Usuario, Integer>{
	
	@Query(value="Select u.* from usuarios u where usuario = ?1 and contrasena = ?2",nativeQuery=true)
	Usuario findByUP(@Param("usuario")String usuario, @Param("contrasena")String contrasena);

}
