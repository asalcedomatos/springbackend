package com.yorch.springboot.apirest.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.yorch.springboot.apirest.models.entity.Solicitudes;

public interface ISolicitudDao extends CrudRepository<Solicitudes, Integer> {

}
