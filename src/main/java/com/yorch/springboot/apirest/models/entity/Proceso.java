package com.yorch.springboot.apirest.models.entity;

import java.io.Serializable;


import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Entity;
import javax.persistence.Table;
/*
import java.util.Set;
import javax.persistence.ManyToMany;
import java.util.HashSet;

import lombok.Data;

@Data 

*/
@Entity
@Table(name="proceso")
public class Proceso implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	private int juzgado; 
	private String tipodeproceso;
	private String Radicadodelproceso;
	private String nombredelproceso;
	private String direcciondeljuzgado;
	private String telefonodeljuzgado;
	private String ciudad;
	private String departamento;
	private String pais;
	private String estadodelproceso;
	
	/*@ManyToMany(mappedBy="procesos")
	private Set<Persona> personas = new HashSet<Persona>();*/
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getJuzgado() {
		return juzgado;
	}
	public void setJuzgado(int juzgado) {
		this.juzgado = juzgado;
	}
	public String gettipodeproceso() {
		return tipodeproceso;
	}
	public void settipodeproceso(String tipodeproceso) {
		this.tipodeproceso = tipodeproceso;
	}
	public String getRadicadodelproceso() {
		return Radicadodelproceso;
	}
	public void setRadicadodelproceso(String radicadodelproceso) {
		Radicadodelproceso = radicadodelproceso;
	}
	public String getNombredelproceso() {
		return nombredelproceso;
	}
	public void setNombredelproceso(String nombredelproceso) {
		this.nombredelproceso = nombredelproceso;
	}
	public String getTelefonodeljuzgado() {
		return telefonodeljuzgado;
	}
	public void setTelefonodeljuzgado(String telefonodeljuzgado) {
		this.telefonodeljuzgado = telefonodeljuzgado;
	}
	public String getCiudad() {
		return ciudad;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	public String getDepartamento() {
		return departamento;
	}
	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getEstadodelproceso() {
		return estadodelproceso;
	}
	public void setEstadodelproceso(String estadodelproceso) {
		this.estadodelproceso = estadodelproceso;
	}
	public String getDirecciondeljuzgado() {
		return direcciondeljuzgado;
	}
	public void setDirecciondeljuzgado(String direcciondeljuzgado) {
		this.direcciondeljuzgado = direcciondeljuzgado;
	}
}
