package com.yorch.springboot.apirest.models.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="etapasprocesales")
public class EtapasProcesales implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private int proceso;
	private String estado_proceso;
	private String recurso_reposicion;
	private String recurso_apelacion;
	private String admision_recurso_apelacion;
	private String etapa_audiencia;
	private String devolver_primer_instancia;
	
	
	public EtapasProcesales() {
		super();
	}
	
	

	public EtapasProcesales(int id, int proceso, String estado_proceso, String recurso_reposicion,
			String recurso_apelacion, String admision_recurso_apelacion, String etapa_audiencia,
			String devolver_primer_instancia) {
		super();
		this.id = id;
		this.proceso = proceso;
		this.estado_proceso = estado_proceso;
		this.recurso_reposicion = recurso_reposicion;
		this.recurso_apelacion = recurso_apelacion;
		this.admision_recurso_apelacion = admision_recurso_apelacion;
		this.etapa_audiencia = etapa_audiencia;
		this.devolver_primer_instancia = devolver_primer_instancia;
	}



	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public int getProceso() {
		return proceso;
	}


	public void setProceso(int proceso) {
		this.proceso = proceso;
	}


	public String getestado_proceso() {
		return estado_proceso;
	}


	public void setestado_proceso(String estado_proceso) {
		this.estado_proceso = estado_proceso;
	}


	public String getrecurso_reposicion() {
		return recurso_reposicion;
	}


	public void setrecurso_reposicion(String recurso_reposicion) {
		this.recurso_reposicion = recurso_reposicion;
	}


	public String getrecurso_apelacion() {
		return recurso_apelacion;
	}


	public void setrecurso_apelacion(String recurso_apelacion) {
		this.recurso_apelacion = recurso_apelacion;
	}


	public String getadmision_recurso_apelacion() {
		return admision_recurso_apelacion;
	}


	public void setadmision_recurso_apelacion(String admision_recurso_apelacion) {
		this.admision_recurso_apelacion = admision_recurso_apelacion;
	}


	public String getetapa_audiencia() {
		return etapa_audiencia;
	}


	public void setetapa_audiencia(String etapa_audiencia) {
		this.etapa_audiencia = etapa_audiencia;
	}


	public String getdevolver_primer_instancia() {
		return devolver_primer_instancia;
	}


	public void setdevolver_primer_instancia(String devolver_primer_instancia) {
		this.devolver_primer_instancia = devolver_primer_instancia;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}
		
}
