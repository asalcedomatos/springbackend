package com.yorch.springboot.apirest.models.service;

import java.util.List;


import com.yorch.springboot.apirest.models.entity.Juzgado;

public interface IJuzgadoService {
public List<Juzgado> findAll();
	
	public Juzgado findById(int id);
	
	public void delete(int id);
	
	public Juzgado save (Juzgado juzgado);

}
