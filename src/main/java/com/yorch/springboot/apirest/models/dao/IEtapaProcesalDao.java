package com.yorch.springboot.apirest.models.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.yorch.springboot.apirest.models.entity.EtapasProcesales;

public interface IEtapaProcesalDao  extends CrudRepository<EtapasProcesales, Long>{

	@Query(value="SELECT * FROM etapasprocesales WHERE proceso =?1", nativeQuery=true)
	List<EtapasProcesales> porProceso(@Param("proceso") int proceso);

}
