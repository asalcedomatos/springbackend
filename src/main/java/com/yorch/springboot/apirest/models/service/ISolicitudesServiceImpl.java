package com.yorch.springboot.apirest.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yorch.springboot.apirest.models.dao.ISolicitudDao;
import com.yorch.springboot.apirest.models.entity.Solicitudes;

@Service
public class ISolicitudesServiceImpl implements ISolicitudesService{
	
	@Autowired
	private ISolicitudDao solicitudDao;
	
	@Override
	public List<Solicitudes> findAll() {
		// TODO Auto-generated method stub
		return (List<Solicitudes>) solicitudDao.findAll();
	}

	@Override
	public Solicitudes findById(int id) {
		// TODO Auto-generated method stub
		return  solicitudDao.findById(id).orElse(null);
	}

	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Solicitudes save(Solicitudes solicitudes) {
		// TODO Auto-generated method stub
		return solicitudDao.save(solicitudes);
	}

}
