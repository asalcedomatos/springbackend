package com.yorch.springboot.apirest.models.service;

import java.util.List;

import com.yorch.springboot.apirest.models.entity.Proceso;

public interface IProcesoService {
	
	public List<Proceso> findAll();
	
	public Proceso findById(int id);
	
	public void delete(int id);
	
	public Proceso save (Proceso proceso);
	
	public List<Proceso> findProcessById(long l);
	
	public List<Proceso> findLawer(long l);

}
