package com.yorch.springboot.apirest.controllers;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.yorch.springboot.apirest.models.entity.Usuario;
import com.yorch.springboot.apirest.models.service.IUsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.yorch.springboot.apirest.models.entity.Persona;
import com.yorch.springboot.apirest.models.entity.Proceso;
import com.yorch.springboot.apirest.models.entity.Respuesta;
//import com.yorch.springboot.apirest.models.entity.Proceso;
//import com.yorch.springboot.apirest.models.entity.Respuesta;
import com.yorch.springboot.apirest.models.service.IPersonaService;
import com.yorch.springboot.apirest.models.service.IProcesoService;
//import com.yorch.springboot.apirest.models.service.IProcesoService;

@CrossOrigin(origins = { "*" },allowedHeaders="*")
@RestController
@RequestMapping("/api")
public class PersonaRestController {
	
	private IPersonaService personaService;
	
	
	@Autowired
	public PersonaRestController(IPersonaService personaService) {
		this.personaService = personaService;
		
	}

	@Autowired
	private IProcesoService procesoService;

	@Autowired
	private IUsuarioService usuarioService;

	@GetMapping("/personas")
	public List<Persona> index() {
		return personaService.findAll();
	}
	
	@GetMapping("/abogados")
	public Respuesta lawers() {
		List<Persona> ps = personaService.abogado();
		return new Respuesta(ps!=null && ps.size()>0 ? 200 : 400, ps!=null && ps.size()>0 ? "Abogados Cargados" : "Ocurrio Un error", ps!=null && ps.size()>0 ? ps : null);
	}

	@GetMapping("/personas/{id}")
	public Persona show(@PathVariable Long id) {
		return personaService.findById(id);
	}
	
	@PostMapping("/personas")
	//@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<?> create(@RequestBody Persona persona,BindingResult result) {
		System.out.println("PERSONA RECIBIDA EN CREATE: "+persona);
		Persona newpersona = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			List<String> errors = result.getFieldErrors().stream()
					.map(err -> "El campo '" + err.getField() + "' " + err.getDefaultMessage())
					.collect(Collectors.toList());
			response.put("erros", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		
		try {
			newpersona = personaService.save(persona);
			if(newpersona != null) {
				Usuario usuario = new Usuario();
				usuario.setContrasena(newpersona.getDocumento());
				usuario.setPersona(newpersona.getId());
				usuario.setUsuario(newpersona.getEmail());

				usuarioService.save(usuario);
			}
		} catch (DataAccessException error) {
			response.put("mensaje", "No se pudo registrar la persona");
			response.put("error", error.getMessage().concat(": ").concat(error.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("mensaje", "la persona ha sido registrado con exito!");
		response.put("registroEntrada", newpersona);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED); 
		
			
	}
	
	@GetMapping("/personaRol/{id}")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public Respuesta getPersona(@PathVariable int id) {
		
		Persona p = (Persona)personaService.findById(id);
		List<Proceso> proc=null;
		if (p!=null) {
			proc =  p.getTipo()=="Cliente" ? procesoService.findLawer(p.getId()) : procesoService.findAll();
		}
		return new Respuesta(proc!=null ? 200 : 400, proc!=null ? "Datos Cargados Correctamente":"Ocurrio Un error", proc!= null ? proc :null);
	}
	
	@PutMapping("/personas/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public Respuesta update(@RequestBody Persona persona, @PathVariable int id) {
		
		Persona actual = (Persona)personaService.findById(id);
		actual.setNombre(persona.getNombre());
		actual.setNombre2(persona.getNombre2());
		actual.setTipo(persona.getTipo());
		actual.setApellido(persona.getApellido());
		actual.setApellido2(persona.getApellido2());
		actual.setDireccion(persona.getDireccion());
		actual.setDocumento(persona.getDocumento());
		actual.setEmail(persona.getEmail());
		actual.setOcupacion(persona.getOcupacion());
		actual.setTelefono(persona.getTelefono());
		actual.setTipopersona(persona.getTipopersona());
		return new Respuesta(200, "palito 200", personaService.save(actual));
	}
}
