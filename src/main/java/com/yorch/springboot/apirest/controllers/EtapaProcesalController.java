package com.yorch.springboot.apirest.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.yorch.springboot.apirest.models.entity.EtapasProcesales;
import com.yorch.springboot.apirest.models.entity.Respuesta;
import com.yorch.springboot.apirest.models.service.IEtapaprocesalService;

@CrossOrigin(origins = { "*" },allowedHeaders="*")
@RestController
@RequestMapping("/api")
public class EtapaProcesalController {
	@Autowired
	private IEtapaprocesalService etapaprocesalService;
	
	@GetMapping("/etapaprocesal")
	public Respuesta getAll() {
		Respuesta respuesta = null;
		List<EtapasProcesales> etapas;
		try {
			etapas = etapaprocesalService.findAll();
			respuesta = new Respuesta(etapas.size()<1 ? 202 : 200, etapas.size()<1 ? "Transaccion exitosa sin datos para mostrar" : "listado Cargado", etapas);
		} catch (Exception e) {
			respuesta = new Respuesta(404, e.getMessage(), null);
		}
		return respuesta;
	}
	
	@GetMapping("/porProceso/{id}")
	public Respuesta porProceso(@PathVariable int id) {
		List<EtapasProcesales> ep = etapaprocesalService.porProceso(id);
		return new Respuesta(ep != null && ep.size()>0 ? 200:400, ep != null && ep.size()>0 ? "Listado Cargado":"ocurrio un error", ep != null && ep.size()>0 ? ep: null);
	}
	
	@GetMapping("/etapaprocesal/{id}")
	@ResponseStatus
	public Respuesta getOne(@PathVariable long id) {
		EtapasProcesales etapa = null;
		Respuesta respuesta = null;
		try {
			etapa = etapaprocesalService.findById(id);
			respuesta = new Respuesta(200, "Etapa Encontrada", etapa);
		} catch (Exception e) {
			respuesta = new Respuesta(400, e.getMessage(), null);
		}
		return respuesta;
	}
	
	@PostMapping("/etapaprocesal")
	@ResponseStatus(HttpStatus.CREATED)
	public Respuesta save(@RequestBody EtapasProcesales etapasProcesales) {
		EtapasProcesales ep = null;
		Respuesta respuesta = null;
		try {
			ep = etapaprocesalService.save( etapasProcesales);
			respuesta = new Respuesta(200, "etapa Guardada", ep);
		} catch (Exception e) {
			respuesta = new Respuesta(400, e.getMessage(), null);
		}
		return respuesta;
	}
}
