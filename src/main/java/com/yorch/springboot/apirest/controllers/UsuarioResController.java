package com.yorch.springboot.apirest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.yorch.springboot.apirest.models.entity.*;

import com.yorch.springboot.apirest.models.service.*;


@CrossOrigin(origins =  "*" ,allowedHeaders="*")
@RestController
@RequestMapping("/api")
public class UsuarioResController {
	@Autowired
	private IUsuarioService usuarioService;
	@Autowired 
	private IPersonaService personaService;
	
	@PostMapping("/login")
	public Respuesta validation(@RequestBody Usuario usuario) {
		Usuario u = usuarioService.findbyUP(usuario.getUsuario(), usuario.getContrasena());
		Persona p = null;
		if (u!=null) {
			p = personaService.findById(u.getPersona());
		}
		return u!=null && p!=null ? new Respuesta(200, "Datos Validados", new Token(p, u)) : new Respuesta(404, "Usuario ó Contraseña incorrectos o vacios", null);
	}
	
	@GetMapping("/login")
	public Respuesta getAll() {
		return usuarioService.findAll();
	}
	
	@GetMapping("/login/{id}")
	public Respuesta getById(@PathVariable int id) {
		return new Respuesta(200, "Usuario Encontrado", usuarioService.findById(id));
	}
	
	@PutMapping("/login/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public Respuesta update(@RequestBody Usuario usuario, @PathVariable int id) {
		Usuario u = (Usuario)usuarioService.findById(id);
		u.setUsuario(usuario.getUsuario()!=null ? usuario.getUsuario() : u.getUsuario());
		u.setContrasena(usuario.getContrasena()!=null ? usuario.getContrasena() : u.getContrasena());
		return new Respuesta(200, "usuario modificado", usuarioService.save(u));
	}


}


