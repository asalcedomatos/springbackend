package com.yorch.springboot.apirest.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.yorch.springboot.apirest.models.entity.Persona;
import com.yorch.springboot.apirest.models.entity.Proceso;
import com.yorch.springboot.apirest.models.entity.Respuesta;
import com.yorch.springboot.apirest.models.service.IProcesoService;

@CrossOrigin(origins = { "*" },allowedHeaders="*")
@RestController
@RequestMapping("/api")
public class ProcesoRestController {
	@Autowired
	private IProcesoService procesoService;
	
	@GetMapping("/procesos")
	public List<Proceso> index() {
		return procesoService.findAll();
	}

	@GetMapping("/procesos/{id}")
	public Proceso show(@PathVariable int id) {
		return procesoService.findById(id);
	}
	
	@PostMapping("/procesos")
	@ResponseStatus(HttpStatus.CREATED)
	public Proceso create(@RequestBody Proceso proceso) {
		System.out.println("proceso RECIBIDA EN CREATE: "+proceso);
		return procesoService.save(proceso);	
	}
	
	@PutMapping("/procesos/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public Proceso update(@RequestBody Proceso proceso, @PathVariable int id) {
		Proceso actual = procesoService.findById(id);
		actual.setJuzgado(proceso.getJuzgado());
		actual.settipodeproceso(proceso.gettipodeproceso());
		actual.setRadicadodelproceso(proceso.getRadicadodelproceso());
		actual.setNombredelproceso(proceso.getNombredelproceso());
		actual.setDirecciondeljuzgado(proceso.getDirecciondeljuzgado());
		actual.setTelefonodeljuzgado(proceso.getTelefonodeljuzgado());
		actual.setCiudad(proceso.getCiudad());
		actual.setDepartamento(proceso.getDepartamento());
		actual.setPais(proceso.getPais());
		actual.setEstadodelproceso(proceso.getEstadodelproceso());
		return procesoService.save(actual);
	}
	
	@PostMapping("/procesoPersona/{id}")
	public Respuesta getPorPersona(@RequestBody Persona persona, @PathVariable int id) {
		return new Respuesta(200, "datos Cargados", persona.getTipo() == "Cliente" ? procesoService.findProcessById(persona.getId()):procesoService.findAll());
	}
	/*
	@GetMapping("/Juzgado/{id}")
	public Respuesta getJuzgado(@RequestBody Persona persona, @PathVariable int id) {
		
		return new Respuesta(200, "llegó algo", persona);
	}*/
}
