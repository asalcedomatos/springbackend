package com.yorch.springboot.apirest.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.yorch.springboot.apirest.models.entity.*;
import com.yorch.springboot.apirest.models.entity.Solicitudes;
import com.yorch.springboot.apirest.models.service.ISolicitudesService;



@CrossOrigin(origins = { "*" },allowedHeaders="*")
@RestController
@RequestMapping("/api")
public class SolicitudesRestController {
	@Autowired
	private ISolicitudesService solicitudesService;
	
	@PostMapping("/solicitudes")
	@ResponseStatus(HttpStatus.CREATED)
	public Respuesta save(@RequestBody Solicitudes solicitudes) {
		System.out.println(solicitudes.getNombre());
		System.out.println(solicitudes.getnombre2());
		System.out.println(solicitudes.getapellido());
		System.out.println(solicitudes.getapellido2());
		System.out.println(solicitudes.getTelefono());
		System.out.println(solicitudes.getCiudad());
		System.out.println(solicitudes.getDepartamento());
		System.out.println(solicitudes.getEmail());
		System.out.println(solicitudes.getdescripcion());
		System.out.println(solicitudes.getocupacion());
		System.out.println(solicitudes.getPais());
		System.out.println(solicitudes.getPersona());
		System.out.println(solicitudes.getAbogado());
		System.out.println(solicitudes.getEstado());
		Solicitudes s = solicitudesService.save(solicitudes);
		return new Respuesta(s!=null ? 200:400, s!=null ? "Solicitud Guardada":"ocurrio un error", s != null ? s : null);
	}
	@GetMapping("/solicitudes")
	public Respuesta getAll() {
		List<Solicitudes> solicitudes = null;
		Respuesta respuesta= null;
		try {
			solicitudes = solicitudesService.findAll();
			respuesta = new Respuesta(200, "listado Cargado", solicitudes);
		} catch (Exception e) {
			respuesta = new Respuesta(400, e.getMessage(), null);
		}
		return respuesta;
	}
	@PutMapping("/solicitudes/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public Respuesta update(@RequestBody Solicitudes solicitudes, @PathVariable int id) {
		Solicitudes s = (Solicitudes)solicitudesService.findById(id);
		s.setNombre(solicitudes.getNombre());
		s.setnombre2(solicitudes.getnombre2());
		s.setapellido(solicitudes.getapellido());
		s.setapellido2(solicitudes.getapellido2());
		s.setTelefono(solicitudes.getTelefono());
		s.setCiudad(solicitudes.getCiudad());
		s.setDepartamento(solicitudes.getDepartamento());
		s.setEmail(solicitudes.getEmail());
		s.setdescripcion(solicitudes.getdescripcion());
		s.setocupacion(solicitudes.getocupacion());
		s.setPais(solicitudes.getPais());
		s.setPersona(solicitudes.getPersona());
		s.setAbogado(solicitudes.getAbogado());
		s.setEstado("Asignada");
		Solicitudes guardada = solicitudesService.save(s);
		return new Respuesta(guardada != null ? 200 : 400, guardada != null ? "solicitud modificada" : "ocurrio un error, no se actualizó el dato", guardada != null ? guardada : null );
	}

}
