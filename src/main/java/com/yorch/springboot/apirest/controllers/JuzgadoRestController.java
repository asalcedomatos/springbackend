package com.yorch.springboot.apirest.controllers;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.yorch.springboot.apirest.models.entity.Juzgado;
import com.yorch.springboot.apirest.models.service.IJuzgadoService;

@CrossOrigin(origins = { "*" },allowedHeaders="*")
@RestController
@RequestMapping("/api")
public class JuzgadoRestController {
	
	@Autowired
	private IJuzgadoService juzgadoService;
	
	@GetMapping("/juzgado")
	public List<Juzgado> index(){
		return juzgadoService.findAll();
	}
	
	@GetMapping("/juzgado/{id}")
	public Juzgado show(@PathVariable int id) {
		return juzgadoService.findById(id);
	}
	/*
	@GetMapping("/Juzgado/{id}")
	public Respuesta getJuzgado(@RequestBody Persona persona, @PathVariable int id) {
		
		return new Respuesta(200, "llegó algo", persona);
	}*/
	
	@PostMapping("/juzgado")
	public Juzgado create(@RequestBody Juzgado juzgado) {
		return juzgadoService.save(juzgado);
	}
	
	@PutMapping("/juzgado/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public Juzgado update(@RequestBody Juzgado juzgado, @PathVariable int id) {
		Juzgado actual = juzgadoService.findById(id);
		actual.setNombre(juzgado.getNombre());
		actual.setDireccion(juzgado.getDireccion());
		actual.setTelefono(juzgado.getTelefono());
		return juzgadoService.save(actual);
	}
}
